USE [master]
GO
/****** Object:  Database [BacchusDB]    Script Date: 14.01.2019 15:04:18 ******/
CREATE DATABASE [BacchusDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BacchusDB', FILENAME = N'C:\Users\olgas\BacchusDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'BacchusDB_log', FILENAME = N'C:\Users\olgas\BacchusDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [BacchusDB] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BacchusDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BacchusDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BacchusDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BacchusDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BacchusDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BacchusDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [BacchusDB] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [BacchusDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BacchusDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BacchusDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BacchusDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BacchusDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BacchusDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BacchusDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BacchusDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BacchusDB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [BacchusDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BacchusDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BacchusDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BacchusDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BacchusDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BacchusDB] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [BacchusDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BacchusDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [BacchusDB] SET  MULTI_USER 
GO
ALTER DATABASE [BacchusDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BacchusDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BacchusDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BacchusDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [BacchusDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [BacchusDB] SET QUERY_STORE = OFF
GO
USE [BacchusDB]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [BacchusDB]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 14.01.2019 15:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bids]    Script Date: 14.01.2019 15:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bids](
	[BidId] [uniqueidentifier] NOT NULL,
	[BidAmount] [real] NOT NULL,
	[ProductId] [uniqueidentifier] NOT NULL,
	[BidTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Bids] PRIMARY KEY CLUSTERED 
(
	[BidId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 14.01.2019 15:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[ProductId] [uniqueidentifier] NOT NULL,
	[ProductName] [nvarchar](200) NOT NULL,
	[ProductDescription] [nvarchar](1000) NOT NULL,
	[BiddingEndDate] [datetime2](7) NOT NULL,
	[ProductCategory] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 14.01.2019 15:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [uniqueidentifier] NOT NULL,
	[FirstName] [nvarchar](200) NOT NULL,
	[LastName] [nvarchar](200) NOT NULL,
	[BidId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190113123513_Initialize', N'2.1.4-rtm-31024')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190113124043_UpdateBidProductRelation', N'2.1.4-rtm-31024')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190113124553_UpdateBidProductRelation', N'2.1.4-rtm-31024')
INSERT [dbo].[Bids] ([BidId], [BidAmount], [ProductId], [BidTime]) VALUES (N'49436a78-eedf-4939-8139-08d67a1d85b7', 22.5, N'b3f6c8ae-6513-45ef-aa40-7051d27829e9', CAST(N'2019-01-14T14:40:51.0000000' AS DateTime2))
INSERT [dbo].[Bids] ([BidId], [BidAmount], [ProductId], [BidTime]) VALUES (N'b34a7889-275b-4698-813a-08d67a1d85b7', 5, N'b3f6c8ae-6513-45ef-aa40-7051d27829e9', CAST(N'2019-01-14T14:41:20.0000000' AS DateTime2))
INSERT [dbo].[Bids] ([BidId], [BidAmount], [ProductId], [BidTime]) VALUES (N'5eeb94a6-3a1b-46ea-813b-08d67a1d85b7', 5, N'1a4afd12-9b89-42fe-8a33-62cf320761a7', CAST(N'2019-01-14T14:41:38.0000000' AS DateTime2))
INSERT [dbo].[Bids] ([BidId], [BidAmount], [ProductId], [BidTime]) VALUES (N'2b650fba-622f-4c8e-813c-08d67a1d85b7', 55, N'1a4afd12-9b89-42fe-8a33-62cf320761a7', CAST(N'2019-01-14T14:42:02.0000000' AS DateTime2))
INSERT [dbo].[Products] ([ProductId], [ProductName], [ProductDescription], [BiddingEndDate], [ProductCategory]) VALUES (N'1a4afd12-9b89-42fe-8a33-62cf320761a7', N'Tracksuit', N'A pig in a poke Tracksuit', CAST(N'2019-01-14T12:46:19.0000000' AS DateTime2), N'Clothing')
INSERT [dbo].[Products] ([ProductId], [ProductName], [ProductDescription], [BiddingEndDate], [ProductCategory]) VALUES (N'b3f6c8ae-6513-45ef-aa40-7051d27829e9', N'Intel Core i7-7700K', N'Merchantable Intel Core i7-7700K', CAST(N'2019-01-14T12:46:19.0000000' AS DateTime2), N'Computer hardware')
INSERT [dbo].[Users] ([UserId], [FirstName], [LastName], [BidId]) VALUES (N'f990ab50-8013-4336-d054-08d67a1d85b2', N'Peter', N'Mihkelson', N'49436a78-eedf-4939-8139-08d67a1d85b7')
INSERT [dbo].[Users] ([UserId], [FirstName], [LastName], [BidId]) VALUES (N'275aa724-82e5-42b6-d055-08d67a1d85b2', N'Anton', N'Simpson', N'b34a7889-275b-4698-813a-08d67a1d85b7')
INSERT [dbo].[Users] ([UserId], [FirstName], [LastName], [BidId]) VALUES (N'77e75dd0-7eaf-47db-d056-08d67a1d85b2', N'Anton', N'Petrov', N'5eeb94a6-3a1b-46ea-813b-08d67a1d85b7')
INSERT [dbo].[Users] ([UserId], [FirstName], [LastName], [BidId]) VALUES (N'b59ba68b-0d66-4cd6-d057-08d67a1d85b2', N'Mari', N'Liis', N'2b650fba-622f-4c8e-813c-08d67a1d85b7')
/****** Object:  Index [IX_Bids_ProductId]    Script Date: 14.01.2019 15:04:19 ******/
CREATE NONCLUSTERED INDEX [IX_Bids_ProductId] ON [dbo].[Bids]
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Users_BidId]    Script Date: 14.01.2019 15:04:19 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Users_BidId] ON [dbo].[Users]
(
	[BidId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Bids]  WITH CHECK ADD  CONSTRAINT [FK_Bids_Products_ProductId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Bids] CHECK CONSTRAINT [FK_Bids_Products_ProductId]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Bids_BidId] FOREIGN KEY([BidId])
REFERENCES [dbo].[Bids] ([BidId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Bids_BidId]
GO
USE [master]
GO
ALTER DATABASE [BacchusDB] SET  READ_WRITE 
GO
