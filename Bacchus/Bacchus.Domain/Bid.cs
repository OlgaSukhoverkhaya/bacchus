﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bacchus.Domain
{
    public class Bid
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid BidId { get; set; }

        [Required]
        public float BidAmount { get; set; }



        [Required]
        public DateTime BidTime { get; set; }

        [Required]
        public Guid ProductId { get; set; }
        [ForeignKey("ProductId")]
        public Product Product { get; set; }

        public User User { get; set; }
    }
}
