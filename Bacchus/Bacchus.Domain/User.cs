﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Bacchus.Domain
{
    public class User
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UserId { get; set; }

        [Required]
        [MinLength(3), MaxLength(200)]
        public string FirstName { get; set; }

        [Required]
        [MinLength(3), MaxLength(200)]
        public string LastName { get; set; }

        [Required]
        public Guid BidId { get; set; }

        public Bid Bid { get; set; }
    }
}
