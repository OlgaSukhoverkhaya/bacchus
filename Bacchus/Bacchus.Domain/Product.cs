﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bacchus.Domain
{
    public class Product
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid ProductId { get; set; }

        [Required]
        [MaxLength(200)]
        public String ProductName { get; set; }

        [Required]
        [MaxLength(1000)]
        public String ProductDescription { get; set; }

        [Required]
        public DateTime BiddingEndDate { get; set; }

        [Required]
        [MaxLength(200)]
        public String ProductCategory { get; set; }

        [InverseProperty("Product")]
        public List<Bid> Bids { get; set; }

        [NotMapped]
        public string BidTimeLeft => CalculateTimeLeftToBidEnd(BiddingEndDate);

        private string CalculateTimeLeftToBidEnd(DateTime endTime)
        {
            string result = string.Empty;

            var now = DateTime.Now;
            var months = ((endTime.Year - now.Year) * 12) + endTime.Month - now.Month;
            TimeSpan span = endTime - now;

            if (months > 2)
            {
                result = months.ToString() + " months left";
            }
            else if (span.TotalDays >= 1)
            {
                var totalDays = span.TotalDays;
                result = string.Format("{0:0} days left", totalDays);
            }
            else
            {
                var totalHours = span.TotalHours;
                var totalMinutes = span.TotalMinutes;
                result = string.Format("{0:0} hours and {0:1} minutes left", totalHours, totalMinutes);
            }

            return result;
        }
    }
}
