﻿using Bacchus.DataStorage.DBContext;
using Bacchus.DataStorage.Implementation;
using Bacchus.Domain;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Bacchus.DataStorage.Tests.Implementation
{
    [TestFixture]
    class BidRepositoryTests
    {
        public BidRepository _bidsRepo { get; set; }
        private User _testUserBid { get; set; }
        private Product _testProduct { get; set; }
        ApplicationContext context { get; set; }
        [SetUp]
        public void SetUp()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>().
                UseInMemoryDatabase(databaseName: "BacchusDB")
                .Options;

            context = new ApplicationContext(options);

            _bidsRepo = new BidRepository(context);

            _testProduct = new Product()
            {
                ProductId = new Guid("50cf320d-9477-4913-bde3-8746f5d70195"),
                ProductName = "Beautiful Socks"
            };

            _testUserBid = new User()
            {
                BidId = new Guid("50cf320d-9477-4913-bde3-8746f5d70196"),
                FirstName = "Viktoria",
                LastName = "Popova",
                Bid = new Bid
                {
                    BidId = new Guid("50cf320d-9477-4913-bde3-8746f5d70196"),
                    BidAmount = 3,
                    BidTime = new DateTime(2018, 10, 12, 22, 10, 23, 222),
                    ProductId = new Guid("50cf320d-9477-4913-bde3-8746f5d70195")
                }
            };


        }

        [Test]
        public async Task AddNewBid_TestBidData_BidIsAddedToDB_DateIsParsedProperly()
        {

            await _bidsRepo.AddNewBidAsync(_testUserBid);

            Assert.That(context.Users, Is.Not.Null);
            Assert.That(context.Users.FirstOrDefault(u => u.FirstName.Equals(_testUserBid.FirstName))
                , Is.Not.Null);
            Assert.That(context.Users.FirstOrDefault(u => u.Bid.ProductId
            .Equals(_testUserBid.Bid.ProductId))
            , Is.Not.Null);

        }

        [Test]
        public async Task UpsertProduct_TesproductData_AddsOnfirstCall_UpdatestOnSecondCall()
        {
            await _bidsRepo.UpsertProductAsync(_testProduct);
            var productEntity = context.Products.FirstOrDefault
                (p => p.ProductId == new Guid("50cf320d-9477-4913-bde3-8746f5d70195"));
            Assert.IsNotNull(productEntity);
            Assert.AreEqual(productEntity.ProductName, _testProduct.ProductName);

            _testProduct.ProductName = "Ugly Socks";
            await _bidsRepo.UpsertProductAsync(_testProduct);
            var updatedProductEntity = context.Products.FirstOrDefault
                (p => p.ProductId == new Guid("50cf320d-9477-4913-bde3-8746f5d70195"));
            Assert.IsNotNull(updatedProductEntity);
            Assert.AreEqual(updatedProductEntity.ProductName, _testProduct.ProductName);

        }

        [Test]
        public async Task GetBidByFullNameAndDateTime_ExistedBidData_BidIsFound()
        {
            await _bidsRepo.AddNewBidAsync(_testUserBid);
            await _bidsRepo.UpsertProductAsync(_testProduct);
            var resultUserBid = await
                _bidsRepo.GetBidByFullNameAndDateTimeAsync(_testUserBid.FirstName,
                _testUserBid.LastName,
                _testUserBid.Bid.BidTime);

            Assert.AreEqual(resultUserBid.BidId, _testUserBid.BidId);
            Assert.AreEqual(resultUserBid.LastName, _testUserBid.LastName);
        }

    }
}
