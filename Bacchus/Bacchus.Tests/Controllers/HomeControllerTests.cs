﻿using Bacchus.Controllers;
using Bacchus.Controllers.Sessions;
using Bacchus.DataStorage;
using Bacchus.DataStorage.Interfaces;
using Bacchus.Domain;
using Bacchus.RestClient.Interfaces;
using Bacchus.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Bacchus.Tests.Controllers
{
    [TestFixture]
    public class HomeControllerTests
    {
        HomeController controller { get; set; }
        Mock<IProductRepository> _productRepoMock { get; set; }
        Mock<IBidRepository> _bidsRepoMock { get; set; }
        List<Product> Products { get; set; }
        Mock<HttpContext> _httpContextMock { get; set; }
        Mock<SessionStateTempDataProvider> _tempDataProviderMock { get; set; }
        Mock<ISessionControl> _sessionControlMock { get; set; }
        private string _data = File.ReadAllText(@"..\..\..\..\Bacchus.RestClient.Tests\Implementation\TestData.json");


        [SetUp]
        public void SetUp()
        {
            Products = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Product>>(_data);
            _productRepoMock = new Mock<IProductRepository>();
            _bidsRepoMock = new Mock<IBidRepository>();

            _tempDataProviderMock = new Mock<SessionStateTempDataProvider>();
            _httpContextMock = new Mock<HttpContext>();
            _sessionControlMock = new Mock<ISessionControl>();

            controller = new HomeController(_productRepoMock.Object, _bidsRepoMock.Object, _sessionControlMock.Object)
            {
                TempData = new TempDataDictionary(_httpContextMock.Object,
              _tempDataProviderMock.Object)
            };

            _productRepoMock.Setup(p =>
                                p.GetProductByIdAsync(new Guid("50cf320d-9477-4913-bde3-8746f5d70195")))
                                .ReturnsAsync(Products[1]);
        }

        [Test]
        public async Task Index_ProductRepoReturnsClothingProduct_IndexReturnsViewResultWithClothingProduct()
        {

            var clothingProducts = Products.Where(p => p.ProductCategory == "Clothing").ToList();

            _productRepoMock.Setup(a => a.GetActualProductsByCategoryAsync("Clothing")).ReturnsAsync(clothingProducts);

            var result = await controller.Index("Clothing") as ViewResult;
            var resultViewModel = result.Model as IndexPageViewModel;
            Assert.That(resultViewModel.Products.Where(p => p.ProductDescription == "Average Long skirt"), Is.Not.Null);
            Assert.That(resultViewModel.Products
                .Where(p => p.ProductId == new Guid("cb06423d-ac61-4fb5-a22a-b6c4421aad0f"))
                , Is.Not.Null);
        }

        [Test]
        public async Task Index_ProductRepoReturnsCategories_IndexReturnsViewResultWithCategories()
        {
            List<string> categories = new List<string>();
            Products.ForEach(p => categories.Add(p.ProductCategory));
            _productRepoMock.Setup(a => a.GetActualProductCategoriesAsync()).ReturnsAsync(categories);

            var result = await controller.Index("") as ViewResult;
            var resultViewModel = result.Model as IndexPageViewModel;
            Assert.That(resultViewModel.Categories.Contains("Clothing"));
            Assert.That(resultViewModel.Categories.Contains("Computer hardware"));
            Assert.That(resultViewModel.Categories.Contains("Sports"));
        }

        [Test]
        public async Task RegisterBid_BidsRepoAddsNewBid_SucessMessageIsAddedToTempDataAsync()
        {
            var _testUserBid = new User()
            {
                FirstName = "Viktoria",
                LastName = "Popova",
                Bid = new Bid
                {
                    BidId = new Guid("50cf320d-9477-4913-bde3-8746f5d70196"),
                    BidAmount = 3,
                    ProductId = new Guid("50cf320d-9477-4913-bde3-8746f5d70195")
                }
            };

            _bidsRepoMock.Setup(b => b.AddNewBidAsync(_testUserBid)).Returns(Task.CompletedTask);

            await controller.RegisterBidAsync(_testUserBid);

            Assert.IsTrue(controller.TempData["BidRegisterResult"].ToString().Contains("Your bid is saved!"));
        }

        [Test]
        public async Task RegisterBid_BidsRepoThrowsException_ErrorMessageIsAddedToTempDataAsync()
        {

            _bidsRepoMock.Setup(b => b.AddNewBidAsync(It.IsAny<User>())).Throws(new DataStorageException());
            await controller.RegisterBidAsync(new User()
            {
                Bid = new Bid()
                { ProductId = new Guid("50cf320d-9477-4913-bde3-8746f5d70195") }
            });

            Assert.IsTrue(controller.TempData["BidRegisterResult"].ToString()
                .Contains("Something went wrong! Your bid is not saved!"));
        }
    }
}
