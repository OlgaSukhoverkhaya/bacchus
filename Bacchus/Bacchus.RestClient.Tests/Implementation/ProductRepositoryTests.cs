﻿using Bacchus.Domain;
using Bacchus.RestClient.Implementation;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bacchus.RestClient.Tests.Implementation
{
    [TestFixture]
    public class ProductRepositoryTests
    {
        private const string _endPoint = "http://test.com/";
        private string _data = File.ReadAllText(@"..\..\..\Implementation\testData.json");
        Mock<HttpMessageHandler> _httpMessageHandler { get; set; }
        HttpClient _client { get; set; }
        ProductRepository _productRepo { get; set; }

        public void SetUpWithTestData()
        {
            _httpMessageHandler = new Mock<HttpMessageHandler>(MockBehavior.Strict);

            _httpMessageHandler.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync",
                   ItExpr.IsAny<HttpRequestMessage>(),
                   ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(_data, Encoding.UTF8, "application/json")
                })
                .Verifiable();

            _client = new HttpClient(_httpMessageHandler.Object)
            {
                BaseAddress = new Uri(_endPoint)
            };

            _productRepo = new ProductRepository(_client);
        }

        public void SetUpWithException()
        {
            _httpMessageHandler = new Mock<HttpMessageHandler>(MockBehavior.Strict);

            _httpMessageHandler.Protected()
                           .Setup<Task<HttpResponseMessage>>("SendAsync",
                              ItExpr.IsAny<HttpRequestMessage>(),
                              ItExpr.IsAny<CancellationToken>())
                              .Throws(new Exception())
                              .Verifiable(); ;

            _client = new HttpClient(_httpMessageHandler.Object);
            _productRepo = new ProductRepository(_client);
        }

        private void SetupWithEmptyStringResponse()
        {
            _httpMessageHandler = new Mock<HttpMessageHandler>(MockBehavior.Strict);

            _httpMessageHandler.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync",
                   ItExpr.IsAny<HttpRequestMessage>(),
                   ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(String.Empty, Encoding.UTF8, "application/json")
                })
                .Verifiable();

            _client = new HttpClient(_httpMessageHandler.Object)
            {
                BaseAddress = new Uri(_endPoint)
            };

            _productRepo = new ProductRepository(_client);
        }

        [Test]
        public async Task GetAllProducts_SetupWithJsonDoc_ListOfProductsFilledFromJsonUsingHttpClient()
        {
            SetUpWithTestData();
            var products = await _productRepo.GetAllProductsAsync();
            Assert.That(products, Is.Not.Null);
            Assert.AreEqual(products.Count, 3);
            Assert.AreEqual(products[2].ProductName, "CORSAIR Vengeance LPX 16GB");
            Assert.AreEqual(products[1].ProductId, new Guid("cb06423d-ac61-4fb5-a22a-b6c4421aad0f"));
        }

        [Test]
        public void GetAllProducts_HttpClientThrowsException_RestClientExceptionIsThrown()
        {
            SetUpWithException();

            Assert.That(async () => await _productRepo.GetAllProductsAsync(),
                Throws.Exception
                .TypeOf<RestClientException>()
                .With.Property("Message")
                .Contains("Error in accesing list of products: ")
                );
        }

        [Test]
        public async Task GetProductById_SetupWithJsonDocAndExistedProductId_ReturnsSelectedProduct()
        {
            SetUpWithTestData();
            var productId = new Guid("cb06423d-ac61-4fb5-a22a-b6c4421aad0f");
            var productFromMethod = await _productRepo.GetProductByIdAsync(productId);

            List<Product> products = JsonConvert.DeserializeObject<List<Product>>(_data);
            var productFromJson = products.FirstOrDefault(p => p.ProductId.Equals(productId));

            Assert.AreEqual(productFromMethod.ProductDescription, productFromJson.ProductDescription);
            Assert.AreEqual(productFromMethod.ProductCategory, productFromJson.ProductCategory);
        }

        [Test]
        public void GetProductById_HttpClientThrowsException_RestClientExceptionIsThrown()
        {
            SetUpWithException();

            Assert.That(async () => await _productRepo.GetProductByIdAsync(new Guid()),
                Throws.Exception
                .TypeOf<RestClientException>()
                .With.Property("Message")
                .Contains("Error in accesing list of products: ")
                );
        }


        [Test]
        public async Task GetActualProducts_ActualProductListIsEmpty_ReturnsEmptyListAsync()
        {
            SetupWithEmptyStringResponse();
            var result = await _productRepo.GetActualProductsAsync();
            Assert.IsTrue(result.GetType() == typeof(List<Product>));
            Assert.IsTrue(result.Count() == 0);
        }

        [Test]
        public async Task GetActualProductCategories_ActualProductListIsEmpty_ReturnsEmptyListAsync()
        {
            SetupWithEmptyStringResponse();
            var result = await _productRepo.GetActualProductCategoriesAsync();
            Assert.IsTrue(result.GetType() == typeof(List<string>));
            Assert.IsTrue(result.Count() == 0);
        }

        [Test]
        public async Task GetProductById_ActualProductListIsEmpty_ReturnsEmptyProduct()
        {
            SetupWithEmptyStringResponse();
            var result = await _productRepo.GetProductByIdAsync(new Guid());
            Assert.IsTrue(result.GetType() == typeof(Product));
            Assert.IsTrue(result.ProductCategory == null);
        }
    }
}
