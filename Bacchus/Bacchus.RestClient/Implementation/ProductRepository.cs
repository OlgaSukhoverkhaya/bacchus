﻿using Bacchus.RestClient.Interfaces;
using System;
using System.Collections.Generic;
using Bacchus.Domain;
using System.Net.Http;
using System.Threading.Tasks;
using System.Linq;

namespace Bacchus.RestClient.Implementation
{
    public class ProductRepository : IProductRepository
    {
        private const string _endPoint = "http://uptime-auction-api.azurewebsites.net/api/Auction";
        private readonly HttpClient _client;
        HttpResponseMessage response = null;

        public ProductRepository(HttpClient client)
        {
            _client = client;
        }

        public async Task<List<Product>> GetAllProductsAsync()
        {
            List<Product> products = new List<Product>();

            try
            {
                products = await DownloadProductsAsync();
            }
            catch (Exception ex)
            {
                throw new RestClientException(ex.Message);
            }
            finally
            {
                DisposeResponse();
            }

            return products;
        }

        public async Task<Product> GetProductByIdAsync(Guid id)
        {
            Product product = new Product();

            var products = await GetAllProductsAsync();
            if (products.Count > 0)
            {
                product = products.FirstOrDefault(p => p.ProductId.Equals(id));
            }

            return product;
        }

        public async Task<List<string>> GetActualProductCategoriesAsync()
        {
            List<string> result = new List<string>();
            var products = await GetActualProductsAsync();

            if (products.Count > 0)
            {
                products.ForEach(p =>
                {
                    if (!result.Contains(p.ProductCategory))
                        result.Add(p.ProductCategory);
                });
            }

            return result;
        }

        private async Task<List<Product>> DownloadProductsAsync()
        {
            List<Product> products = new List<Product>();
            response = await _client.GetAsync(_endPoint);

            if (response.IsSuccessStatusCode)
            {
                products = await response.Content.ReadAsAsync<List<Product>>()
                    ?? new List<Product>();
            }

            return products;
        }

        public async Task<List<Product>> GetActualProductsByCategoryAsync(string category)
        {
            List<Product> products = await GetActualProductsAsync();
            var result = products
                .Where(p => p.ProductCategory.Equals(category))
                .ToList();

            return result;
        }

        public async Task<List<Product>> GetActualProductsAsync()
        {
            List<Product> products = await GetAllProductsAsync();
            if (products.Count > 0)
            {
                products = products
                    .Where(p => p.BiddingEndDate > DateTime.Now)
                    .ToList();
            }

            return products;
        }

        private void DisposeResponse()
        {
            if (response != null)
            {
                ((IDisposable)response).Dispose();
            }
        }


    }
}
