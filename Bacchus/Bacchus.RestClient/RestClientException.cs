﻿using System;

namespace Bacchus.RestClient
{
    public class RestClientException : Exception
    {
        public RestClientException()
        {

        }

        public RestClientException(string message) 
            : base("Error in accesing list of products: " + message)
        {

        }

        public RestClientException(string message, Exception inner) 
            : base("Error in accesing list of products: " + message, inner)
        {

        }
    }
}
