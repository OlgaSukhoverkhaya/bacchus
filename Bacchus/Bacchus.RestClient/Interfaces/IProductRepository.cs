﻿using Bacchus.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bacchus.RestClient.Interfaces
{
    public interface IProductRepository
    {
        Task<List<Product>> GetAllProductsAsync();
        Task<List<string>> GetActualProductCategoriesAsync();
        Task<Product> GetProductByIdAsync(Guid id);
        Task<List<Product>> GetActualProductsByCategoryAsync(string category);
        Task<List<Product>> GetActualProductsAsync();
    }
}
