﻿using System;

namespace Bacchus.DataStorage
{
    public class DataStorageException : Exception
    {
            public DataStorageException()
            {

            }

            public DataStorageException(string message)
                : base(message)
            {

            }

            public DataStorageException(string message, Exception inner)
                : base(message, inner)
            {

            }
    }
}
