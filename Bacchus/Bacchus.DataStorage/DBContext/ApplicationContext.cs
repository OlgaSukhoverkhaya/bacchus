﻿using Bacchus.Domain;
using Microsoft.EntityFrameworkCore;

namespace Bacchus.DataStorage.DBContext
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        { }

        public DbSet<User> Users { get; set; }
        public DbSet<Bid> Bids { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
