﻿using Bacchus.DataStorage.DBContext;
using Bacchus.DataStorage.Interfaces;
using Bacchus.Domain;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Bacchus.DataStorage.Implementation
{
    public class BidRepository : IBidRepository
    {
        private readonly ApplicationContext _context;
        private DbSet<User> _users;
        private DbSet<Product> _products;

        public BidRepository(ApplicationContext context)
        {
            _context = context;
            _users = _context.Users;
            _products = _context.Products;
        }

        private List<User> _userBidsList { get; set; } = new List<User>();

        public async Task AddNewBidAsync(User user)
        {
            User currentUserBid =
                await GetBidByFullNameAndDateTimeAsync(user.FirstName, user.LastName, user.Bid.BidTime)
                 ?? null;
            try
            {
                if (currentUserBid == null)
                {
                    user.Bid.BidTime = user.Bid.BidTime.AddTicks(-(user.Bid.BidTime.Ticks % TimeSpan.TicksPerSecond));
                    await _users.AddAsync(user);
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                throw new DataStorageException("Error in adding bid data to database: " + ex.Message);
            }
        }

        public async Task UpsertProductAsync(Product product)
        {
            try
            {
                if (product != null)
                {
                    if (_products.Any(p => p.ProductId == product.ProductId))
                    {
                        _products.Update(product);
                    }
                    else
                    {
                        await _products.AddAsync(product);
                    }

                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                throw new DataStorageException("Error in Adding/Updating product: " + ex.Message);
            }
        }



        public async Task<User> GetBidByFullNameAndDateTimeAsync(string firstName, string lastName, DateTime bidTime)
        {
            await GetAllUsers();
            User result = null;

            if (_userBidsList.Count > 0)
            {
                result = _users.Include(u => u.Bid)
                    .Include(p => p.Bid.Product)
                    .Where(u => u.FirstName.Equals(firstName))
                    .Where(u => u.LastName.Equals(lastName))
                    .FirstOrDefault(u => u.Bid.BidTime.Equals(bidTime));
            }

            return result;
        }

        private async Task GetAllUsers()
        {
            try
            {
                if (_users != null)
                {
                    _userBidsList = await _users.ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new DataStorageException("Error occured while accesing bids data from database: " + ex.Message);
            }
        }

        public async Task<List<User>> GetExpiredBidsAsync()
        {
            List<User> expiredBids = new List<User>();
            try
            {
                expiredBids = await _users.Include(b => b.Bid)
                    .Include(p => p.Bid.Product)
                    .Where(b => b.Bid.BidTime < DateTime.Now)
                    .ToListAsync();
            }
            catch (Exception ex)
            {
                throw new DataStorageException("Error occured while accesing expired bids: " + ex.Message);
            }
            return expiredBids;
        }
    }
}
