﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Bacchus.Domain;

namespace Bacchus.DataStorage.Interfaces
{
    public interface IBidRepository
    {
        Task AddNewBidAsync(User user);
        Task<User> GetBidByFullNameAndDateTimeAsync(string firstName, string lastName, DateTime bidTime);
        Task UpsertProductAsync(Product product);
        Task<List<User>> GetExpiredBidsAsync();
    }
}
