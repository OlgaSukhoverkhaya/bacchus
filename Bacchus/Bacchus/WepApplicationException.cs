﻿using System;

namespace Bacchus
{
    public class WepApplicationException : Exception
    {
        public WepApplicationException()
        {
        }

        public WepApplicationException(string message) : base(message)
        {
        }

        public WepApplicationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
