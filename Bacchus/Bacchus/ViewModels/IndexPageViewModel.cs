﻿using Bacchus.Domain;
using System;
using System.Collections.Generic;

namespace Bacchus.ViewModels
{
    public class IndexPageViewModel
    {
        public List<string> Categories { get; set; } = new List<string>();
        public List<Product> Products { get; set; } = new List<Product>();
        public Product CurrentProduct { get; set; } = new Product();
        public User CurrentUserBid { get; set; } = new User();
        public string BidRegisterResult { get; set; } = string.Empty;
        public bool IsRegistrationFormHidden { get; set; } = true;
    }
}
