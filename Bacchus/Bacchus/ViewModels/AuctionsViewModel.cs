﻿using Bacchus.Domain;
using System;
using System.Collections.Generic;

namespace Bacchus.ViewModels
{
    public class AuctionsViewModel
    {
        public List<User> UserBids { get; set; } = new List<User>();
        public List<Guid> AuctionWinners { get; set; } = new List<Guid>();
        public string Message { get; set; } = string.Empty;
    }
}
