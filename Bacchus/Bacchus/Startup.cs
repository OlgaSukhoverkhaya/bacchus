﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Bacchus.Controllers.Sessions;
using Bacchus.DataStorage.DBContext;
using Bacchus.DataStorage.Implementation;
using Bacchus.DataStorage.Interfaces;
using Bacchus.Domain;
using Bacchus.RestClient.Implementation;
using Bacchus.RestClient.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Bacchus
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            string connection = Configuration.GetConnectionString("DefaultConnection");

            services.AddDbContext<ApplicationContext>(
                options => options.UseSqlServer(connection,
                m => m.MigrationsAssembly("Bacchus")));

            services.AddMvc();
            services.AddSingleton<HttpClient>();
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<IBidRepository, BidRepository>();
            services.AddTransient<ISessionControl, SessionControl>();
            services.AddDistributedMemoryCache();
            services.AddSession();

            services.Configure<RazorViewEngineOptions>(o =>
            o.ViewLocationFormats.Add("/Views/{0}" + RazorViewEngine.ViewExtension)
                );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSession();
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}");
            });
        }
    }
}
