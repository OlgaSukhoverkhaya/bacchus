﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bacchus.DataStorage.Interfaces;
using Bacchus.Domain;
using Bacchus.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Bacchus.Controllers
{
    public class AuctionResultsController : Controller
    {
        private IBidRepository _bidRepo;
        private AuctionsViewModel _viewModel { get; set; } = new AuctionsViewModel();

        public AuctionResultsController(IBidRepository bidRepo)
        {
            _bidRepo = bidRepo;
        }

        public async Task<IActionResult> AuctionResultsView()
        {
            List<User> expiredAuctions = await _bidRepo.GetExpiredBidsAsync();

            if (expiredAuctions != null)
            {
                _viewModel.UserBids = expiredAuctions;
                GetWinnerIdsFromUserBidList(expiredAuctions);
            }

            return View(_viewModel);
        }

        private void GetWinnerIdsFromUserBidList(List<User> userBids)
        {
            List<Guid> result = new List<Guid>();
            List<Guid> productIds = userBids.Select(b => b.Bid.ProductId).Distinct().ToList();

            foreach (var item in productIds)
            {
                var winner = userBids.Where(p => p.Bid.ProductId == item)
                    .OrderByDescending(b=>b.Bid.BidAmount).First();

                result.Add(winner.UserId);
            }

            _viewModel.AuctionWinners = result;
        }
    }
}