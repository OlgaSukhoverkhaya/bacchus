﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Bacchus.Controllers.Sessions;
using Bacchus.DataStorage.Interfaces;
using Bacchus.Domain;
using Bacchus.RestClient.Interfaces;
using Bacchus.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Bacchus.Controllers
{
    public class MyAuctionsController : Controller
    {
        private IBidRepository _bidRepo;
        private ISessionControl _sessionControl;
        private string _message => "Nothing was found!";

        private AuctionsViewModel _viewModel { get; set; } = new AuctionsViewModel();
        public MyAuctionsController(IBidRepository bidRepo,
            ISessionControl sessionControl)
        {
            _bidRepo = bidRepo;
            _sessionControl = sessionControl;
        }

        public async Task<IActionResult> MyAuctionsView(string userKey)
        {
            string sessionData = string.Empty;
            sessionData = _sessionControl.GetUserDataFromSession(HttpContext);

            if (userKey != null && userKey != string.Empty)
            {
                await GetBidByUserKeyAsync(userKey);
            }
            if (sessionData != null)
            {
                await GetBidByUserKeyAsync(sessionData);
            }

            return View(_viewModel);
        }

        private async Task GetBidByUserKeyAsync(string userIdentity)
        {
            try
            {
                await SearchBidByUserKeyAsync(userIdentity);
            }
            catch (Exception ex)
            {
                if (ex is IndexOutOfRangeException || ex is FormatException)
                {
                    _viewModel.Message = _message;
                }
                else
                {
                    throw new WepApplicationException("Error occured while bid setting to view model: " + ex.Message);
                }
            }

        }

        private async Task SearchBidByUserKeyAsync(string userIdentity)
        {
            string[] data = userIdentity.Split("/");

            User userBid = null ??
                await _bidRepo.GetBidByFullNameAndDateTimeAsync(data[0], data[1], DateTime.Parse(data[2]));

            if (userBid != null)
            {
                //checks is same bid exists in viewmodel
                User testbid = _viewModel.UserBids
                    .Where(u => u.FirstName == userBid.FirstName)
                    .Where(u => u.LastName == userBid.LastName)
                    .FirstOrDefault(u => u.Bid.BidTime == userBid.Bid.BidTime);

                if (testbid == null)
                {
                    _viewModel.UserBids.Add(userBid);
                    _viewModel.Message = "";
                }
            }
            else
            {
                _viewModel.Message = _message;
            }
        }

    }
}