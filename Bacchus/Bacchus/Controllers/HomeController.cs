﻿using Bacchus.Controllers.Sessions;
using Bacchus.DataStorage.Interfaces;
using Bacchus.Domain;
using Bacchus.RestClient.Interfaces;
using Bacchus.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Bacchus.Controllers
{
    public class HomeController : Controller
    {
        private IProductRepository _productRepo;
        private IBidRepository _bidRepo;
        private ISessionControl _sessionControl;
        public IndexPageViewModel _viewModel { get; set; } = new IndexPageViewModel();

        public HomeController(IProductRepository productRepo,
            IBidRepository bidRepo,
            ISessionControl sessionControl)
        {
            _productRepo = productRepo;
            _bidRepo = bidRepo;
            _sessionControl = sessionControl;
        }


        public async Task<IActionResult> Index(string productsCategory)
        {
            try
            {
                _viewModel.Categories = await _productRepo.GetActualProductCategoriesAsync();
                await SortProductsDependsOnCategory(productsCategory);
                _viewModel.BidRegisterResult = TempData["BidRegisterResult"]?.ToString() ?? string.Empty;
                await SetCurrentProductAsync();
            }
            catch (Exception ex)
            {
                throw new WepApplicationException("Error in setting data to IndexPageViewModel: " + ex.Message);
            }

            return View(_viewModel);

        }

        private async Task SetCurrentProductAsync()
        {
            if (TempData["CurrentProduct"] != null)
            {
                var productId = TempData["CurrentProduct"].ToString();

                _viewModel.CurrentProduct =
                                await _productRepo.GetProductByIdAsync(new Guid(productId));

                _viewModel.IsRegistrationFormHidden = false;

                TempData.Clear();
            }
        }

        private async Task SortProductsDependsOnCategory(string productCategory)
        {
            if (productCategory != null && productCategory != string.Empty)
            {
                _viewModel.Products = await _productRepo.GetActualProductsByCategoryAsync(productCategory);
            }
            else
            {
                _viewModel.Products = await _productRepo.GetActualProductsAsync();
            }
        }

        public IActionResult ShowProduct(string productId)
        {
            TempData["CurrentProduct"] = productId;
            return RedirectToAction("Index");
        }


        public async Task<IActionResult> RegisterBidAsync(User userBid)
        {
            userBid.Bid.BidTime = DateTime.Now;
            var addBidResult = string.Empty;

            var productId = userBid.Bid.ProductId;
            var product = await _productRepo.GetProductByIdAsync(productId);

            try
            {
                await _bidRepo.UpsertProductAsync(product);
                await _bidRepo.AddNewBidAsync(userBid);

                var userIdentity = _sessionControl.SaveUserToSession(userBid, HttpContext);
                addBidResult = "Your bid is saved!\n" +
                    "To check bid state copy this key: " + userIdentity;
            }
            catch (Exception)
            {
                addBidResult = "Something went wrong! Your bid is not saved!";
                if (product.BiddingEndDate < userBid.Bid.BidTime)
                {
                    addBidResult = addBidResult + "Auction is ended!";
                }
            }

            TempData["BidRegisterResult"] = addBidResult;
            return RedirectToAction("Index");
        }


    }
}
