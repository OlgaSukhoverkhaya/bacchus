﻿using Bacchus.Domain;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bacchus.Controllers.Sessions
{
    public class SessionControl : ISessionControl
    {
        private const string _userKey = "UserKey";

        public string SaveUserToSession(User user, HttpContext httpContext)
        {
            string userIdentity = string.Empty;

            try
            {
                userIdentity = $"{user.FirstName}/{user.LastName}/{user.Bid.BidTime}";
                httpContext.Session.SetString(_userKey, userIdentity);
            }
            catch (Exception ex)
            {
                throw new WepApplicationException("Error in saving user to session! " + ex.Message);
            }

            return userIdentity;
        }

        public string GetUserDataFromSession(HttpContext httpContext)
        {
            string userIdentity = string.Empty;

            try
            {
                userIdentity = httpContext.Session.GetString(_userKey);
            }
            catch (Exception ex)
            {
                throw new WepApplicationException("Error in accesing user data is session! " + ex.Message);
            }

            return userIdentity;
        }
    }
}
