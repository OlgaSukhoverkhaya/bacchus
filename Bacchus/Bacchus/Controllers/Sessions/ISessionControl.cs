﻿using Bacchus.Domain;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bacchus.Controllers.Sessions
{
    public interface ISessionControl
    {
        string SaveUserToSession(User user, HttpContext httpContext);
        string GetUserDataFromSession(HttpContext httpContext);
    }
}
