﻿//Scrolling into Bid registration form
$(function () {
    if ($('#dinamic-form').css('display') === "block") {
        $('#registration-lbl').get(0).scrollIntoView();
    }
});

//Bidding end time timer
$(function CountTimeToBidEnd() {
    var dateFromHtml = $("#end-date-timer").attr("name");
    var formattedDate = moment(dateFromHtml, 'DD.MM.YYYY HH:mm:ss');
    var date = formattedDate.toDate();
    var countDownDate = date.getTime();
    console.log(formattedDate);
    console.log(date);
    console.log(new Date());

    var x = setInterval(function () {
        var now = new Date().getTime();
        var distance = countDownDate - now;
        //milisec*sec*min*hours %-reminder after dividing
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor(distance % (1000 * 60) / 1000);

        if (days < 0) days = 0;
        if (hours < 0) hours = 0;
        if (minutes < 0) minutes = 0;
        if (seconds < 0) seconds = 0;

        $("#end-date-timer").html($("<b>"
            + days + "days "
            + hours + "hours "
            + minutes + "minutest "
            + seconds + "seconds"
            + "</b>"));
    });
});

//Only float values for input field
$('.number').on('input', function () {
    this.value = this.value.replace(/[^0-9,]/g, '').replace(/(,.*?),(.*,)?/, "$1");;
});